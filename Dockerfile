FROM openjdk:21

ARG ARTIFACT_ID=string-microservice
ARG ARTIFACT_VERSION=1.0.0
ENV EXECUTABLE_ARTIFACT=${ARTIFACT_ID}-${ARTIFACT_VERSION}.jar

ENV DEPLOYMENT_DIR=/opt/kafka/streams

ENV BOOTSTRAP_SERVER_URL="localhost:9092"
ENV INPUT_TOPIC_NAME="input-topic"
ENV OUTPUT_TOPIC_NAME="output-topic"

ENV OTEL_ENABLED=false
ENV OTEL_AGENT=lib/opentelemetry-javaagent.jar

ENV JMX_ENABLED=false
# ENV JMX_EXTRA=""
ENV JMX_PORT=9989

# ENV CONFIG_FILE=""
ENV LOG_FILE=${DEPLOYMENT_DIR}/config/log4j2.xml

WORKDIR ${DEPLOYMENT_DIR}

COPY src/main/resources/log4j2.xml ${DEPLOYMENT_DIR}/config/log4j2.xml
COPY opentelemetry-javaagent.jar ${DEPLOYMENT_DIR}/lib/opentelemetry-javaagent.jar
COPY target/lib ${DEPLOYMENT_DIR}/lib
COPY target/${EXECUTABLE_ARTIFACT} ${DEPLOYMENT_DIR}
COPY files/run ${DEPLOYMENT_DIR}

CMD ["sh", "/opt/kafka/streams/run"]
