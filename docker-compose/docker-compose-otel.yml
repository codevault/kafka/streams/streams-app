---
version: '3.8'

services:
  application:
    image: string-microservice:v1
    hostname: application
    container_name: application
    depends_on:
      - broker
    environment:
      BOOTSTRAP_SERVER_URL: "broker:29092"
      # INPUT_TOPIC_NAME: "input-topic"
      # OUTPUT_TOPIC_NAME: "output-topic"
      # LOG_FILE: /opt/kafka/streams/config/log4j2.xml
      OTEL_ENABLED: true
      # OTEL_AGENT: lib/opentelemetry-javaagent.jar
      OTEL_TRACES_EXPORTER: otlp
      OTEL_METRICS_EXPORTER: otlp
      OTEL_EXPORTER_OTLP_ENDPOINT: http://collector:5555
      OTEL_RESOURCE_ATTRIBUTES: service.name=string-microservice,service.version=1.0.0
    volumes:
      - type: bind
        source: ./log4j2.xml
        target: /opt/kafka/streams/config/log4j2.xml
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 512M
        reservations:
          cpus: '0.25'
          memory: 256M

  zookeeper:
    image: confluentinc/cp-zookeeper:7.5.0
    hostname: zookeeper
    container_name: zookeeper
    ports:
      - "2181:2181"
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000

  broker:
    image: confluentinc/cp-kafka:7.5.0
    hostname: broker
    container_name: broker
    depends_on:
      - zookeeper
    ports:
      - "29092:29092"
      - "9092:9092"
      - "9101:9101"
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://broker:29092,PLAINTEXT_HOST://localhost:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
      KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
      KAFKA_JMX_PORT: 9101
      KAFKA_JMX_HOSTNAME: localhost

  collector:
    image: otel/opentelemetry-collector:0.92.0
    container_name: collector
    hostname: collector
    depends_on:
      tempo:
        condition: service_healthy
      prometheus:
        condition: service_healthy
    command: [ "--config=/etc/collector-config.yaml" ]
    volumes:
      - ./otel-config/collector-config-local.yaml:/etc/collector-config.yaml
    ports:
      - "5555:5555"
      - "6666:6666"

  tempo:
    image: grafana/tempo:1.5.0
    command: [ "-search.enabled=true", "-config.file=/etc/tempo.yaml" ]
    container_name: tempo
    hostname: tempo
    volumes:
      - ./otel-config/tempo-config.yaml:/etc/tempo.yaml
      - ./otel-config/tempo-overrides.yaml:/etc/overrides.yaml
      - ./tempo-data:/tmp/tempo
    ports:
      - "3200:3200"
      - "4317:4317"
    healthcheck:
      interval: 5s
      retries: 10
      test: wget --no-verbose --tries=1 --spider http://localhost:3200/status || exit 1

  prometheus:
    image: prom/prometheus:v2.49.1
    container_name: prometheus
    hostname: prometheus
    command:
      - --config.file=/etc/prometheus.yaml
      - --web.enable-remote-write-receiver
      - --enable-feature=exemplar-storage
    volumes:
      - ./otel-config/prometheus.yaml:/etc/prometheus.yaml
    ports:
      - "9090:9090"
    healthcheck:
      interval: 5s
      retries: 10
      test: wget --no-verbose --tries=1 --spider http://localhost:9090/status || exit 1

  grafana:
    image: grafana/grafana:10.0.10
    container_name: grafana
    hostname: grafana 
    depends_on:
      tempo:
        condition: service_healthy
      prometheus:
        condition: service_healthy
    volumes:
      - ./otel-config/grafana-bootstrap.ini:/etc/grafana/grafana.ini
      - ./otel-config/grafana-datasources.yaml:/etc/grafana/provisioning/datasources/datasources.yaml
    environment:
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
      - GF_AUTH_DISABLE_LOGIN_FORM=true
    ports:
      - "3000:3000"
    healthcheck:
      interval: 5s
      retries: 10
      test: wget --no-verbose --tries=1 --spider http://localhost:3000 || exit 1

networks:
  default:
    name: string-microservice
