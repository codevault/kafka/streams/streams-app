#!/bin/bash

mvn clean package -Dmaven.test.skip=true

# Build application docker image
docker build --progress=plain --no-cache -t string-microservice:v1 .

# Run application with messaging and monitoring infrastructure
docker compose -f docker-compose/docker-compose-jmx.yml up -d
