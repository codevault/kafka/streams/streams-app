#!/bin/bash

mvn clean package -Dmaven.test.skip=true

AGENT_VERSION=v1.32.0
AGENT_FILE=opentelemetry-javaagent.jar
if [ ! -f "${AGENT_FILE}" ]; then
  curl -L https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/download/${AGENT_VERSION}/opentelemetry-javaagent.jar --output ${AGENT_FILE}
fi

export OTEL_TRACES_EXPORTER=otlp
export OTEL_METRICS_EXPORTER=otlp
export OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:5555
export OTEL_RESOURCE_ATTRIBUTES=service.name=string-microservice,service.version=1.0.0

java -javaagent:./${AGENT_FILE} -jar target/string-microservice-1.0.0.jar
