# Kafka Streams String Microservice
This application is a simple kafka streams application which processes string records.</br>
This streams app receives a KeyValue<String, String> record as input, logs the received record to console and forwards the record to the output.

## Application File Paths
* Kafka Streams App: src/main/java/com/learning/kafka/streams/StringKafkaStream.java
* Kafka Streams Test: src/test/java/com/learning/kafka/streams/StringKafkaStreamTest.java
* Kafka Client Consumer: src/main/java/com/learning/kafka/streams/simulator/EventConsumer.java
* Kafka Client Producer: src/main/java/com/learning/kafka/streams/simulator/EventProducer.java
* Application Config: src/main/java/com/learning/kafka/streams/admin/AppConfig.java
* Topic Creation: src/main/java/com/learning/kafka/streams/topic/CreateTopics.java

### Log Config 
* PATH: src/main/resources/log4j2.xml

### How to run Kafka Streams String Microservice?
* Start Kafka Docker -> docker compose -f docker-compose/docker-compose-kafka.yml up -d
* Start Kafka Streams App -> StringKafkaStream
* Start Kafka Client Consumer -> EventConsumer
* Run Kafka Client Producer -> EventProducer

# Kafka Streams String Microservice - OpenTelemetry
This application has open telemetry trace and metrics instrumentation added.

## OTel Config
* PATH: docker-compose/otel-config
  * Prometheus: prometheus.yaml
  * Tempo: tempo-config.yaml, tempo-overrides.yaml
  * OTel Collector: collector-config-local.yaml
  * Grafana: grafana-bootstrap.ini, grafana-datasources.yaml

## OTel Endpoints:
* Prometheus: http://localhost:9090/status (only used to check if promethium is alive)
* Tempo: http://localhost:3200/status (only used to check if tempo is alive)
* Grafana: http://localhost:3000 (tempo traces and promethium metrics can be viewed here)

### How to View Telemetry data in Grafana
* Select Dashboard -> Add Visualization -> x
* DataSource -> Prometheus
  * Metrics -> select any -> Run queries
* DataSource -> Tempo
  * QueryType -> Search
    * ServiceName -> string-microservice
    * SpanName -> input-topic process
* Time -> Last 5 minutes(loads data faster)

### How to run Kafka Streams String Microservice with OTel?
* Start Kafka Docker -> docker compose -f docker-compose/docker-compose-kafka.yml up -d
* Start OTel Docker -> docker compose -f docker-compose/docker-compose-monitor.yml up -d
* Start Streams Application -> run-app-local.sh
* Start Kafka Client Consumer -> EventConsumer
* Run Kafka Client Producer -> EventProducer
* View Telemetry data in Grafana

### How to run Kafka Streams String Microservice with OTel in Docker?
* Important Environment Variables(added in DockerFile and docker-compose.yml):
  * OTEL_ENABLED: true
  * OTEL_AGENT: lib/opentelemetry-javaagent.jar
  * OTEL_TRACES_EXPORTER: otlp
  * OTEL_METRICS_EXPORTER: otlp
  * OTEL_EXPORTER_OTLP_ENDPOINT: http://collector:5555
  * OTEL_RESOURCE_ATTRIBUTES: service.name=string-microservice,service.version=1.0.0
* Run Application Script -> run-app-docker-otel.sh (run from WSL)
* Start Kafka Client Consumer -> EventConsumer
* Run Kafka Client Producer -> EventProducer
* View Telemetry data in Grafana

### How to run Kafka Streams String Microservice with JMX in Docker?
* Important Environment Variables(added in DockerFile and docker-compose.yml):
  * JMX_ENABLED: true
  * JMX_PORT: 9989
  * JMX_EXTRA: '-Djava.rmi.server.hostname=127.0.0.1 -Dcom.sun.management.jmxremote.host=0.0.0.0 -Dcom.sun.management.jmxremote.rmi.port=9989'
    * Important: -Dcom.sun.management.jmxremote.rmi.port=9989 should be same as JMX_PORT:9989
* Run Application Script -> run-app-docker-jmx.sh (run from WSL)
* Start VisualVM -> visualvm.exe --jdkhome "C:\Program Files\Java\jdk-21.0.1"
* Connect Application to VisualVM
  * File -> Add JMX Connection...
    * Connection -> localhost:9989
    * Check -> Do not use SSL connection
    * Select -> Ok
* Start Kafka Client Consumer -> EventConsumer
* Run Kafka Client Producer -> EventProducer
* View Telemetry data in Grafana

# Extra Scripts
## To run streams application
```shell
mvn clean package -Dmaven.test.skip=true
java -jar target/kafka-streams-string-app-1.0.0.jar
```

## To run streams application with open telemetry data printing to application console logs
```shell
mvn clean package -Dmaven.test.skip=true

AGENT_FILE=opentelemetry-javaagent-all.jar
if [ ! -f "${AGENT_FILE}" ]; then
  curl -L https://github.com/aws-observability/aws-otel-java-instrumentation/releases/download/v1.28.1/aws-opentelemetry-agent.jar --output ${AGENT_FILE}
fi

export OTEL_TRACES_EXPORTER=logging
export OTEL_METRICS_EXPORTER=logging

java -javaagent:./${AGENT_FILE} -jar target/kafka-streams-string-app-1.0.0.jar
```

### Reference: 
* Youtube: https://www.youtube.com/playlist?list=PLDqi6CuDzubz5viRapQ049TjJMOCCu9MJ
* CodeBase: https://github.com/build-on-aws/instrumenting-java-apps-using-opentelemetry
* BlogPost: https://community.aws/tutorials/instrumenting-java-apps-using-opentelemetry