package com.learning.kafka.streams.simulator;

import com.learning.kafka.streams.config.AppConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public class EventConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventConsumer.class);

    private static final Supplier<Properties> PROPERTIES = () -> {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVER_URL.get());
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "output-group");
        return properties;
    };

    public static void main(String[] args) {

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(PROPERTIES.get());
        consumer.subscribe(Collections.singletonList(AppConfig.OUTPUT_TOPIC_NAME.get()));

        AtomicBoolean shutdownRequested = new AtomicBoolean(false);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            shutdownRequested.set(true);
            consumer.wakeup();
        }));

        try {
            while (!shutdownRequested.get()) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record : records) {
                    LOGGER.info("Received message: key = {}, value = {}, topic = {}, partition = {}, offset = {}",
                            record.key(), record.value(), record.topic(), record.partition(), record.offset());
                }
            }
        } catch (WakeupException e) {
            LOGGER.warn("WakeupException: {}", e.getMessage());
        } finally {
            consumer.close();
        }
    }
}
