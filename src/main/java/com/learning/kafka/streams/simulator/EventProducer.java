package com.learning.kafka.streams.simulator;

import com.learning.kafka.streams.config.AppConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.function.Supplier;

public class EventProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventProducer.class);

    private static final Supplier<Properties> PROPERTIES = () -> {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVER_URL.get());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return properties;
    };

    public static void main(String[] args) {

        String key = "key";
        String value = "value_";

        Producer<String, String> producer = new KafkaProducer<>(PROPERTIES.get());
        for (int i = 0; i < 10000000; i++) {
            producer(producer, key, value + i);
        }
        producer.close();
    }

    private static void producer(Producer<String, String> producer, String key, String value) {
        ProducerRecord<String, String> record = new ProducerRecord<>(AppConfig.INPUT_TOPIC_NAME.get(), key, value);
        producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                if (exception == null) {
                    LOGGER.info("Send message: key = {}, value = {}, topic: {}, partition: {}, offset: {}",
                            key, value, metadata.topic(), metadata.partition(), metadata.offset());
                } else {
                    LOGGER.error("Error sending message: {}", exception.getMessage());
                }
            }
        });
    }
}
