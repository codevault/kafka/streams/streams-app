package com.learning.kafka.streams.config;

import java.util.Optional;
import java.util.function.Supplier;

public class AppConfig {

    public static final Supplier<String> APPLICATION_NAME = () -> "string-microservice";
    public static final Supplier<String> BOOTSTRAP_SERVER_URL =
            () -> Optional.ofNullable(System.getenv("BOOTSTRAP_SERVER_URL")).orElse("localhost:9092");
    public static final Supplier<String> INPUT_TOPIC_NAME =
            () -> Optional.ofNullable(System.getenv("INPUT_TOPIC_NAME")).orElse("input-topic");
    public static final Supplier<String> OUTPUT_TOPIC_NAME =
            () -> Optional.ofNullable(System.getenv("OUTPUT_TOPIC_NAME")).orElse("output-topic");

    private AppConfig() {
        // hidden constructor
    }
}
