package com.learning.kafka.streams.topic;

import com.learning.kafka.streams.config.AppConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

public class CreateTopics {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTopics.class);

    public static final Supplier<List<NewTopic>> NEW_TOPIC_LIST = () -> {
        List<NewTopic> newTopics = new ArrayList<>();
        newTopics.add(new NewTopic(AppConfig.INPUT_TOPIC_NAME.get(), 1, (short) 1));
        newTopics.add(new NewTopic(AppConfig.OUTPUT_TOPIC_NAME.get(), 1, (short) 1));
        return newTopics;
    };

    private static final Supplier<Properties> PROPERTIES = () -> {
        Properties properties = new Properties();
        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVER_URL.get());
        return properties;
    };

    private static boolean topicExists(AdminClient adminClient, String topicName) throws ExecutionException, InterruptedException {
        return adminClient.listTopics().names().get().contains(topicName);
    }

    public static void createTopics() {
        try (AdminClient adminClient = AdminClient.create(PROPERTIES.get())) {
            for (NewTopic topic : NEW_TOPIC_LIST.get()) {
                String topicName = topic.name();
                if (!topicExists(adminClient, topicName)) {
                    adminClient.createTopics(Collections.singletonList(topic)).all().get();
                    LOGGER.info("Topic {} created successfully!", topicName);
                } else {
                    LOGGER.warn("Topic {} already exists. Skipping creation.", topicName);
                }
            }
        } catch (ExecutionException | InterruptedException e) {
            LOGGER.error("Exception: ", e);
        }
    }
}
