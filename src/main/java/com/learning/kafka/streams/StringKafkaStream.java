package com.learning.kafka.streams;

import com.learning.kafka.streams.config.AppConfig;
import com.learning.kafka.streams.topic.CreateTopics;
import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.context.Scope;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class StringKafkaStream {

    private static final Logger LOGGER = LoggerFactory.getLogger(StringKafkaStream.class);

    private static int eventsPerSec;
    private static final AtomicInteger counter = new AtomicInteger(0);

    private static final String scopeNameTrace = "string.microservice.trace";
    private static final String scopeVersionTrace = "1.0.0";
    private static final Tracer tracer = GlobalOpenTelemetry.getTracer(scopeNameTrace, scopeVersionTrace);

    private static final String scopeNameMeter = "string.microservice.meter";
    private static final String scopeVersionMeter = "1.0.0";
    private static final Meter meter = GlobalOpenTelemetry.meterBuilder(scopeNameMeter)
            .setInstrumentationVersion(scopeVersionMeter)
            .build();

    private static final LongCounter numberOfExecutions;

    static {
        numberOfExecutions = meter.counterBuilder("custom.metric.number.of.exec")
                .setDescription("This metric captures the number of executions of the stream peek processor")
                .setUnit("int")
                .build();

        meter.gaugeBuilder("custom.metric.events.per.sec")
                .setDescription("This metric captures the number of events processed per second")
                .setUnit("int")
                .buildWithCallback(
                        r -> {
                            r.record(eventsPerSec);
                        });

        meter.gaugeBuilder("custom.metric.heap.memory")
                .setDescription("This metric captures the heap memory size")
                .setUnit("byte")
                .buildWithCallback(
                        r -> {
                            r.record(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                        });
    }

    public static final Supplier<Properties> PROPERTIES = () -> {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfig.APPLICATION_NAME.get());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVER_URL.get());
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        return properties;
    };

    public static Topology getTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        builder.stream(AppConfig.INPUT_TOPIC_NAME.get(), Consumed.with(Serdes.String(), Serdes.String()))
                .peek(StringKafkaStream::peek)
                .to(AppConfig.OUTPUT_TOPIC_NAME.get(), Produced.with(Serdes.String(), Serdes.String()));

        return builder.build();
    }

    private static void peek(String key, String value) {
        Span span = tracer.spanBuilder("peek.logger").startSpan();
        try (Scope scope = span.makeCurrent()) {
            LOGGER.info("Key: {}, Value: {}, Count: {}, EventsPerSec: {}",
                    key, value, counter.incrementAndGet(), eventsPerSec);
            numberOfExecutions.add(1);
        } finally {
            span.end();
        }
        sleep();
    }

    @WithSpan
    private static void sleep() {
//        try {
//            Thread.sleep(Duration.ofMillis(10).toMillis());
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
    }

    public static void main(String[] args) {
        CreateTopics.createTopics();
        scheduler();

        KafkaStreams streams = new KafkaStreams(getTopology(), PROPERTIES.get());
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    public static void scheduler() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Runnable task = () -> eventsPerSec = counter.getAndSet(0);
        scheduler.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
    }
}
