package com.learning.kafka.streams;

import com.learning.kafka.streams.config.AppConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

public class StringKafkaStreamTest {

    private TopologyTestDriver testDriver;
    private TestInputTopic<String, String> inputTopic;
    private TestOutputTopic<String, String> outputTopic;

    @BeforeEach
    public void setUp() {
        Properties properties = StringKafkaStream.PROPERTIES.get();
        Topology topology = StringKafkaStream.getTopology();

        testDriver = new TopologyTestDriver(topology, properties);

        inputTopic = testDriver.createInputTopic(AppConfig.INPUT_TOPIC_NAME.get(),
                new StringSerializer(), new StringSerializer());

        outputTopic = testDriver.createOutputTopic(AppConfig.OUTPUT_TOPIC_NAME.get(),
                new StringDeserializer(), new StringDeserializer());
    }

    @AfterEach
    public void tearDown() {
        testDriver.close();
    }

    @Test
    public void testKafkaStreams() {
        String key = "key";
        String value = "value";

        inputTopic.pipeInput(key, value);
        KeyValue<String, String> result = outputTopic.readKeyValue();

        Assertions.assertEquals(key, result.key);
        Assertions.assertEquals(value, result.value);
    }
}
