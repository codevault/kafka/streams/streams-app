#!/bin/bash

mvn clean package -Dmaven.test.skip=true

AGENT_VERSION=v1.32.0
AGENT_FILE=opentelemetry-javaagent.jar
if [ ! -f "${AGENT_FILE}" ]; then
  curl -L https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/download/${AGENT_VERSION}/opentelemetry-javaagent.jar --output ${AGENT_FILE}
fi

# Build application docker image
docker build --progress=plain --no-cache -t string-microservice:v1 .

# Run application with messaging and monitoring infrastructure
docker compose -f docker-compose/docker-compose-otel.yml up -d
